const deliveries = require ('./../public/output/deliveries.json');
const matches = require ('./../public/output/matches.json');
const fs = require ('fs');

const allTeamData2015 = matches.filter (key => {
    if (key.season === '2015') {
        return true;
    }
})

const teamDataOf2015 = deliveries.filter ((key) => {
    return key.match_id >= allTeamData2015[0].id && key.match_id <= allTeamData2015[allTeamData2015.length - 1].id;
});

// let allBowlers = {};

// teamDataOf2015.forEach ((key) => {
//     if (allBowlers.hasOwnProperty (key.bowler)) {
//         allBowlers[key.bowler] += parseInt(key.total_runs);
//     }else {
//         allBowlers[key.bowler] = parseInt(key.total_runs);
//     }
// });

// teamDataOf2015.forEach ((key) => {
//     if (allBowlers.hasOwnProperty(key.bowler)) {
//         allBowlers[key.bowler] /= parseInt(key.over);
//     }
// });

// teamDataOf2015.forEach ((key) => {
//     if (allBowlers.hasOwnProperty(key.bowler)) {
//         allBowlers[key.bowler] /= (parseInt(key.ball) / 6);
//     }
// });

// const array = Object.keys(allBowlers).map(key => allBowlers[key]);

// const top10EconomicalBowler = allBowlersEconomy.filter ((name, index) => {
//     return index < 10;
// });


// Object.entries (allBowlers).sort ((a, b) => b[1] - a[1]);

// fs.writeFileSync ("./public/output/problem4.json", JSON.stringify(top10EconomicalBowler), "utf-8", (err) => {
//     if (err) {
//         console.log (err);
//     }
// });

const data2015 = teamDataOf2015.map ((key) => {
    return {
        "bowler" : key.bowler,
        "economy" : (parseInt(key.total_runs) / (parseInt (key.over) + (parseInt (key.ball) / 6))).toFixed (3),
    }
});

const allBowlers = [];

data2015.forEach ((key) => {
    if (!allBowlers.includes (key.bowler)) {
        allBowlers.push (key.bowler);
    }
})

const economyRate = ((name, obj) => {
    let economyRate = 0;
    obj.forEach ((key) => {
        if (key.bowler === name) {
            economyRate += parseFloat (key.economy);
        }
    });
    return {
        "bowler" : name, 
        "economy" : economyRate,
    }
})

let economy = [];

allBowlers.forEach ((key) => {
    economy.push (economyRate (key, data2015));
})

economy.sort (function (a, b) {
    return b.economy - a.economy
})

const top10economy = economy.filter ((key, index) => {
    return index < 10;
})

console.log (top10economy);

fs.writeFileSync ('./public/output/problem4.json', JSON.stringify (top10economy), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
})


