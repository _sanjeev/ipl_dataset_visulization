const csv = require('csvtojson');
const filePath = "./../src/data/matches.csv";
const fs = require('fs');

csv().fromFile(filePath).then((jsonObject) => {
    // console.log (jsonObject);

    let season = [];

    for (let index = 0; index < jsonObject.length; index++) {
        if (!season.includes(jsonObject[index].season)) {
            season.push(jsonObject[index].season);
        }
    }

    console.log(season);

    let output = [];

    for (let index = 0; index < season.length; index++) {
        let matchWonPerTeam = {};

        jsonObject.forEach((key) => {
            if (matchWonPerTeam.hasOwnProperty(key.winner)) {
                matchWonPerTeam[key.winner]++;
            } else {
                matchWonPerTeam[key.winner] = 1;
            }
        });
        output.push (matchWonPerTeam);
    }

    // console.log (output);

    fs.writeFileSync ('./../public/output/problem2.json', JSON.stringify (output), "utf-8", (err) => {
        if (err) {
            console.log (err);
        }
    });

})
