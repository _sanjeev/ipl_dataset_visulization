const csv = require ("csvtojson");
const filePath = "./src/data/matches.csv";
const fs = require ('fs');

csv().fromFile(filePath).then((jsonObject) => {
    // console.log (jsonObject);

    let noOfMatchesPlayed = {};

    jsonObject.forEach ((key) => {
        if (noOfMatchesPlayed.hasOwnProperty(key.season)) {
            noOfMatchesPlayed[key.season]++;
        } else {
            noOfMatchesPlayed[key.season] = 1;
        }
    });

    console.log (noOfMatchesPlayed);

    fs.writeFileSync ('./public/output/problem1.json', JSON.stringify (noOfMatchesPlayed), "utf-8", (err) => {
        if (err) {
            console.log (err);
        }
    });


}); 