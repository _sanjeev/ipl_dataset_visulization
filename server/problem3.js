const deliveries = require ('../public/output/deliveries.json')
const matches = require ('../public/output/matches.json');
const fs = require ('fs');


const allTeamData2016 = matches.filter ((key) => {
    return key.season === '2016';
});

// console.log (allTeamData2016[0]);

const teamData2016 = deliveries.filter ((key) => {
    return key.match_id >= allTeamData2016[0].id && key.match_id <= allTeamData2016[allTeamData2016.length - 1].id;
});

let extraRunPerTeam2016 = {};

teamData2016.forEach ((key) => {
    if (extraRunPerTeam2016.hasOwnProperty (key.bowling_team)) {
        extraRunPerTeam2016[key.bowling_team] += parseInt(key.extra_runs);
    } else {
        extraRunPerTeam2016[key.bowling_team] = parseInt(key.extra_runs);
    }
})

console.log (extraRunPerTeam2016);

fs.writeFileSync('./public/output/problem3.json', JSON.stringify (extraRunPerTeam2016), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
});