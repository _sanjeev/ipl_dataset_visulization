# Hi, I'm Sanjeev Kumar Singh! 👋
---


# IPL dataset visualization

Data Analysis with IPL match-by-match dataset from ball by ball cricket data.

Dataset has been downloaded from Kaggle and it can be found here: https://www.kaggle.com/manasgarg/ipl.

The dataset contains two files: deliveries.csv and matches.csv. The file used for this analysis is matches.csv & deliveries.csv.

---


## Authors

- [@_sanjeev](https://gitlab.com/_sanjeev)

---


## Install

---

**1.Install Node**

https://nodejs.org/en/download/

**2. Install Git**

Window :- https://gitforwindows.org/

Linux :- https://git-scm.com/download/linux

Mac :- https://git-scm.com/download/mac

**3. Install VSCode**

https://code.visualstudio.com/download

**4. Clone this repository**

```git
git clone https://gitlab.com/_sanjeev/ipl_dataset_visulization.git
```
**5. Install NPM Package**

```git
npm install
```

**6. Install Live Server**

```git
npm install -g live-server
```

**7. Start live server**

```git
live-server
```

**8. Visualize results on your browser by opening html File**

http://127.0.0.1:8080/

**9. Run JavaScript File in the terminal of vs code**

```jajascript
node <filename>.js
```
---
# Project Structure

**`src/data`**: This directory contains the dataset: **`matches.csv`** and **`deliveries.csv`**.

**`server`**: This directory contains all your JavaScript logic.

**`public`**: This directory is contains all the HTML and CSS file required to visualize the results. The static server serves the **`.html`** file present in this directory.

**`public/output`**: This directory contain all the **`.json`** file.

**`node_modules`**: This directory is used by node and npm to store third party packages like `csvtojson` and `live-server`.

**`index.js`**: This file contains the code which: 1. Reads the csv data 2. Calls the JavaScript business logic functions. 3. Saves the results in `public/output/matches.json`

**`ipl.js`**: This file contains the code which: 1. Reads the csv data 2. Calls the JavaScript business logic functions. 3. Saves the results in `public/output/deliveries.json`

---

# Problems

There should be 2 files:

**`deliveries.csv`**
**`matches.csv`**
In this data assignment you will transform raw data of IPL to calculate the following stats:

1.Number of matches played per year for all the years in IPL. 

2.Number of matches won per team per year in IPL.

3.Extra runs conceded per team in the year 2016.

4.Top 10 economical bowlers in the year 2015.

Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder

---


## 🔗 Links

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/sanjeev-kumar-singh-91474a1a3/)

---

## 🚀 About Me
I'm working on full stack developer...






